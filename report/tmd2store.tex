%===============================================================================
% Reliability analysis of a 2 DoF system with an added TMD
%
\section{Analysis of a 2-DOF system}
\subsection{Definition of the limit state function}
  \begin{figure}[htb]
    \centering
    \includegraphics[width=6cm]{tmd2store_model}
    \caption{Lumped model of a 2-DOF system with a TMD.}
    \label{fig:tmd2store-model}
  \end{figure}

  In order to do a reliability analysis of a tuned-mass damper
  applied to a 2 DOF system subject, it is first necessary to
  create a model that will be used in the formulation of
  the limit state function. In this case, we will study
  the system shown in fig. \ref{fig:tmd2store-model},
  comprised of two masses interconnected by springs.
  The 1st natural frequency was calculated afterward
  and then a TMD was annexed to the model. The parameters
  $m_d$, $k_d$ and $c_d$ were calculated following the
  procedures detailed in \cite{Soom1983} for optimal
  design of TMD apllied to the nominal values of
  $m_1$, $m_2$, $k_1$ and $k_2$.
  The dynamic equations of the complete system then
  becomes

  \begin{align}
    \label{eq:tmd2store-eq-mck}
    & \begin{bmatrix}
      m_1 &   0 &   0 \\
        0 & m_2 &   0 \\
        0 &   0 & m_d
    \end{bmatrix}
    \begin{bmatrix}
      \ddot{x}_1 \\
      \ddot{x}_2 \\
      \ddot{x}_d
    \end{bmatrix} +
    \begin{bmatrix}
      0 &    0 &   0  \\
      0 &  c_d & -c_d \\
      0 & -c_d &  c_d
    \end{bmatrix}
    \begin{bmatrix}
      \dot{x}_1 \\
      \dot{x}_2 \\
      \dot{x}_d
    \end{bmatrix} \nonumber \\
    & \qquad +
    \begin{bmatrix}
      k_1+k_2 &    -k_2 &   0  \\
         -k_2 & k_2+k_d & -k_d \\
            0 &    -k_d &  k_d
    \end{bmatrix}
    \begin{bmatrix}
      x_1 \\
      x_2 \\
      x_d
    \end{bmatrix} =
    \begin{bmatrix}
      0    \\
      F(t) \\
      0
    \end{bmatrix},
  \end{align}
  which, in matrix notation, is equivalent to
  \begin{equation}
    \label{eq:tmdstore-eq-mat}
    \*M \ddot{\*x} +
    \*C  \dot{\*x} +
    \*K \*x =
    \*F.
  \end{equation}

  Were the damping matrix guaranteed to be proportional, then it
  would be possible to calculate the system's eigenvectors and
  perform its modal analysis right at this stage in order to
  compute its FRF. Such may not be true, though, so the more
  general approach of converting to the state-space formulation
  will be made. It follows then

  \begin{align*}
    \dot{\*z} &= \*A \*z + \*B \*u \\
          \*y &= \*C \*z + \*D \*u,
  \end{align*}
  where $\*z = [\*x \dot{*x}]$ and 
  \begin{align*}
    \*A & =
    \begin{bmatrix}
      \*0                 & \*I \\
      \*M^{-1} \*K & \*M^{-1} \*C
    \end{bmatrix} &
    \*B & =
    \begin{bmatrix}
      \*0 \\
      \*M^{-1} \*F
    \end{bmatrix} \\
    \*C & = 
    \begin{bmatrix}
      \*I & \*0
    \end{bmatrix} &
    \*D & = \*0
  \end{align*}
  Theoretically, the matrix of FRF may be computed from the formula
  \begin{equation}
    \*H(\omega) = \*C(\*A - \omega \*I)^{-1} \*B,
    \label{eq:stspace-frf}
  \end{equation}
  but for computational
  purposes, it will be numerically calculated using the function
  \texttt{freqrep} from MATLAB.
  
  The inclusion of a TMD tuned to the first mode will flatten the
  magnitude of the FRF on the neighbourhood of the first natural
  frequency by removing the original peak and adding two smaller
  ones nearby, as can be seen in fig.\ \ref{fig:FRF22}.
  Consider a frequency range $[L, U]$ that contains those two
  peaks.
  Then define, for a given realization $\*x$ of the system,
  \begin{equation}
    A_p(\*x) = \max_{L \leq \omega \leq U}
      \left| H_{2,2}(\omega) \right|,
  \end{equation}
  that is, the greatest amplitude the 2nd storey will oscillate with
  given an harmonic excitation $F$ on it.

  It is then necessary to stablish a formula that will determine whether
  the system is behaving satisfactorily or not. In this case,
  a maximum allowed amplitude $A_{max}$ will be defined, so that
  the function
  \begin{equation}
    \label{eq:tmd2store-limst}
    g(\*x) = A_{max} - A_p(\*x)
  \end{equation}
  will assume negative values when the greatest magnitude in the
  frequency range overcomes the maximum allowed amplitude of
  oscillation. Now that the limit-state function is determined,
  it is possible to proceed to reliability analysis.

  \begin{figure}[htb]
    \centering
    \begin{subfigure}{12cm}
      \includegraphics[width=\textwidth]{tmd2store_FRF22mag_12x4}
    \end{subfigure} \\
    \begin{subfigure}{12cm}
      \includegraphics[width=\textwidth]{tmd2store_FRF22phs_12x4}
    \end{subfigure}
    \caption{Frequency reponse of the second storey to harmonic force excitation F.}
    \label{fig:FRF22}
  \end{figure}

  For the numerical model, the following parameters were used:
  
  \begin{table}[htb]
    \centering
    \caption{Parameter values for the 2DOF model.}
    \label{tab:tmd2store-param}
    \begin{tabular}{rcc} \toprule
      Parameter & Value & Unit \\ \midrule
      $\bar{M}_1$  &  \num{1e3} & \si{\kilo\gram} \\
      $\delta_{M_1}$ & 0.10 & \\
      $\bar{M}_2$  &  \num{1e3} & \si{\kilo\gram} \\
      $\delta_{M_2}$ & 0.10 & \\
      $K_1$  &  \num{1e6} & \si{\N\per\metre} \\
      $K_2$  &  \num{1e6} & \si{\N\per\metre} \\ \bottomrule
    \end{tabular}
  \end{table}

  The amplitude of oscillation of the 2nd storey $H_{2,2}$ was
  normalized by the static deflection of the building, as done
  in \cite{Beck2015}. Then we a maximum allowed amplitude of
  $A_{max} = 10$ static deflections was stablished.
  The values $L = \SI{14}{\radian\per\second}$ and
  $U = \SI{24}{\radian\per\second}$ define the interval where
  the peak of the FRF is searched.

  Since this case in particular only considers the uncertainty on two
  variables, it is possible to plot the failure surface of the
  limit-state function defined in \ref{eq:tmd2store-limst} and check
  the shape of its failure domains. Figure \ref{fig:tmd2store-failsurf}
  reveals the existence of several design points. This happens
  because the appropriate operation of a TMD requires that its natural
  frequency and the frequency of the target mode be close to
  one another, if not equal. Then the values of the random
  parameters in the reliability problem must sit close enough
  to their respective mean values. Consequently, each variable
  must be in a two-sided interval. For this reason, methods
  originally tailored towards one-sided problems, such as
  FORM, may deliver results which are not
  representative.

  \begin{figure}[htb]
    \centering
    \includegraphics[width=12cm]{tmd2store_failsurf_16x10}
    \caption{Failure surface of the 2-DOF system}
    \label{fig:tmd2store-failsurf}
  \end{figure}

\subsection{Design points}
  Despite the somewhat complex shape of the failure domain, the
  FORM was still used here in order to find the exact values of
  the different design points.
  The following approach was made in order to search for the
  design points:
  \begin{enumerate}
    \item generate 100 random starting points;
    \item for each of these 100 starting points:
      \begin{enumerate}
        \item run the FORM method;
        \item save the design point and its corresponding reliability index;
      \end{enumerate}
    \item plot the design points that were found against their respective
      reliability index.
  \end{enumerate}

  The starting points were generated according to a uniform distribution
  of the range $[\mu_j - 8\sigma_j, \mu_j + 8\sigma_j]$, that
  is, $\pm 8$ standard deviations from the mean of each random
  variable.

  \begin{figure}[htb]
    \centering
    \includegraphics[width=12cm]{tmd2store_FORM_attractors_10x10}
    \caption{Results from running FORM for 100 random starting points.}
    \label{fig:tmd2store_attractors}
  \end{figure}

  The results of those 100 runs of the FORM method are shown in
  fig.\ \ref{fig:tmd2store_attractors}, where each color represents a
  design point, the vertical axes denotes the values $M_j^*$ the random
  variables take at their respective design point and the vertical
  axes denote the reliability index of the respective design points.

  \begin{figure}[htb]
    \centering
    \includegraphics[width=12cm]{tmd2store_failsurf_dp_12x12}
    \caption{Failure surface with design points indicated in red, blue and green dots. The black cross indicates the mean point.}
    \label{fig:tmd2store_failsurf_attractors}
  \end{figure}

  The same three design points in the range $\mu\pm 8\sigma$ are
  highlighted in fig.\ \ref{fig:tmd2store_failsurf_attractors},
  with the black cross indicating the mean value of each $M_j$.
  The values of $M_j$ for each design point, as well as $P_f$,
  were organized in tab.\ \ref{tab:tmd2store-dp}.

  \begin{table}[htb]
    \caption{Design points of the reliability problem of the 2 DOF system.}
    \label{tab:tmd2store-dp}
    \centering
    \begin{tabular}{lrrr} \toprule
      Design Point & $M_1$ & $M_2$ &      $P_f$ \\ \midrule
      1            & 1080  & 1260  &  3.1780e-3 \\
      2            &  855  &  748  &  1.8379e-3 \\
      3            &  853  &  602  &  1.1295e-5 \\ \bottomrule
    \end{tabular}
  \end{table}

\subsection{Crude Monte Carlo simulation}

  Since a representative value of $P_f$ cannot be obtained from a single
  run of the FORM on the 2 DOF system, a crude Monte Carlo simulation
  was used. In order to speed the running time up, the failure surface
  were approximated by a spline interpolation on a rectangular grid
  of points generated from 101 points regularly spaced on
  $[\mu_1 - \sigma_1, \mu_1 + \sigma_1]$ and 101 points regularly
  spaced on $[\mu_2 - \sigma_2, \mu_2 + \sigma_2]$.

  Then the crude Monte Carlo simulation with Latin Hypercube sampling
  was used to create a surrogate model. The Monte Carlo simulation converged
  after \num{1.950e7} system realizations to the probability
  $P_f = \num{5.1097e-3}$ with a $CoV = 0.01$.

  Note that, if we approximate the failure domains by straight lines,
  we obtain
  \begin{align*}
    & \num{5.1097e-3} \approx \num{5.005e-3} = \\
    & \qquad \num{3.1780e-3} + \num{1.8379e-3} - \num{1.1295e-5},
  \end{align*}
  that is, in this particular case the probability obtained from the
  Monte Carlo simulation equals the correction done by combining the
  probabilities obtained from FORM.

  \begin{figure}[htb]
    \centering
    \includegraphics[width=12cm]{tmd2store_MCS_Pf_16x8}
    \caption{Convergence history of MCS applied to the 2-DOF system.}
  \end{figure}
