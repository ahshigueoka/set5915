%===============================================================================
% Reliability analysis of a 20-storey lumped model of a building
%
\section{Analysis of a 20-storey building}
\begin{figure}[htb]
  \centering
  \includegraphics[width=80mm]{tmdnstore_model}
  \caption{Lumped model of a 20-storey building with a TMD.}
  \label{fig:tmd20store-model}
\end{figure}
This study case will consider the lumped-model of a 20-storey building
with base excitation,with a tuned-mass damper target to the first mode
being attached to the structure. The reliability analysis will then
verify how reliable the operation of the TMD is in regard to uncertainties
in the mass and the stiffness of the storeys.

\subsection{Formulation of the limit-state function}
The building was modelled as a series of 20 mass elements interconnected
by springs, as shown in fig.\ \ref{fig:tmd20store-model}. 
The dynamic equations may be written in matrix form as

\begin{align*}
  & \begin{bmatrix}
       M_1 &      0 & \cdots &      0 &      0 \\
         0 &    M_2 & \cdots &      0 &      0 \\
    \vdots & \vdots & \ddots & \vdots & \vdots \\
         0 &      0 & \cdots & M_{20} &      0 \\
         0 &      0 & \cdots &      0 &    M_d
  \end{bmatrix}
  \begin{bmatrix}
    \ddot{x}_1 \\
    \ddot{x}_2 \\
    \vdots     \\
    \ddot{x}_{20} \\
    \ddot{x}_d
  \end{bmatrix} + 
  \begin{bmatrix}
         0 &      0 & \cdots &      0 &      0 \\
         0 &      0 & \cdots &      0 &      0 \\
    \vdots & \vdots & \ddots & \vdots & \vdots \\
         0 &      0 & \cdots & c_{20} &      0 \\
         0 &      0 & \cdots &      0 &    c_d
  \end{bmatrix}
  \begin{bmatrix}
    \dot{x}_1 \\
    \dot{x}_2 \\
    \vdots     \\
    \dot{x}_{20} \\
    \dot{x}_d 
  \end{bmatrix} \\
  &+\begin{bmatrix}
    K_1+K_2 &    -K_2 & \cdots &          0 &      0 \\
       -K_2 & K_2+K_3 & \cdots &          0 &      0 \\
     \vdots &  \vdots & \ddots &     \vdots & \vdots \\
          0 &       0 & \cdots & K_{20}+K_d &   -K_d \\
          0 &       0 & \cdots &       -K_d &    K_d
  \end{bmatrix}
  \begin{bmatrix}
    x_1 \\
    x_2 \\
    \vdots     \\
    x_{20} \\
    x_d
  \end{bmatrix} = 
  \begin{bmatrix}
    K_1 x_0 \\
    0       \\
    \vdots  \\
    0       \\
    0
  \end{bmatrix},
\end{align*}
which, just like in the previous case, may be rewritten as
\begin{equation*}
  \*M \ddot{\*x} + \*C \dot{\*x} + \*K \*x = \*F x_1(t),
\end{equation*}
where
\begin{equation*}
  \*M =
  \begin{bmatrix}
       M_1 &      0 & \cdots &      0 &      0 \\
         0 &    M_2 & \cdots &      0 &      0 \\
    \vdots & \vdots & \ddots & \vdots & \vdots \\
         0 &      0 & \cdots & M_{20} &      0 \\
         0 &      0 & \cdots &      0 &    M_d
  \end{bmatrix}
\end{equation*}
\begin{equation*}
  \*C =
  \begin{bmatrix}
         0 &      0 & \cdots &      0 &      0 \\
         0 &      0 & \cdots &      0 &      0 \\
    \vdots & \vdots & \ddots & \vdots & \vdots \\
         0 &      0 & \cdots & c_{20} &      0 \\
         0 &      0 & \cdots &      0 &    c_d
  \end{bmatrix}
\end{equation*}
\begin{equation*}
  \*K =
  \begin{bmatrix}
    K_1+K_2 &    -K_2 & \cdots &          0 &      0 \\
       -K_2 & K_2+K_3 & \cdots &          0 &      0 \\
     \vdots &  \vdots & \ddots &     \vdots & \vdots \\
          0 &       0 & \cdots & K_{20}+K_d &   -K_d \\
          0 &       0 & \cdots &       -K_d &    K_d
  \end{bmatrix}
\end{equation*}
\begin{equation*}
  \*F =
  \begin{bmatrix}
    K_1    \\
    0      \\
    \vdots \\
    0      \\
    0
  \end{bmatrix}
\end{equation*}
The FRF of each storey relative to $X_0(\omega)$ may be obtained from
eq.\ \ref{eq:stspace-frf}, but for practical purposes the computation
of the limit state function will compute the FRF numerically.

In order to account for the peak of oscillation in the neighbourhood
of the operation of the TMD, define, for a given system realization $\*x$,
\begin{equation*}
  A_p(\*x) = \max_{L \leq \omega \leq U}
    \left| X_{20, 0}(\omega) - X_0 \right|,
\end{equation*}
that is, the maximum amplitude of oscillation of the 20th storey
relative to the ground in the frequency range $[L, U]$.
Aditionalyy, define the maximum allowed amplitude of oscillation $A_{max}$
so that the limit-state function
\begin{equation*}
  g(\*x) = A_{max} - A_p(\*x)
\end{equation*}
will be negative the amplitude of the oscillation of the 20th storey,
relative to the ground, becomes unnaceptable.

\subsection{Design points}
  The reliability problem of this case considers as random
  variable not only the mass of each storey but also their
  stifness, totalizing 40 variables. It is impossible to assess
  the existence of multiple different design points when
  there are more than two random variables, so in this case
  the most practical alternative is to run the FORM algorithm
  from several different starting points and then collect
  the design point that will be found.

  \begin{figure}[htb]
    \centering
    \begin{subfigure}{12cm}
      \includegraphics[width=\textwidth]{tmdfrf_FORM_attractors_Mj_16x12.pdf}
      \caption{Equivalent mass of each storey at different design points.}
      \label{fig:tmdfrf-attractors-mj}
    \end{subfigure}
    \begin{subfigure}{12cm}
      \includegraphics[width=\textwidth]{tmdfrf_FORM_attractors_Kj_16x12.pdf}
      \caption{Equivalent stiffness of each storey at different design points.}
      \label{fig:tmdfrf-attractors-kj}
    \end{subfigure}
    \caption{Design points for each value of $beta$. Each color represents a different design point.}
  \end{figure}

  In the present example, only two different design points
  were detected, as shown in fig.\ \ref{fig:tmdfrf-attractors-mj}
  and \ref{fig:tmdfrf-attractors-kj}. The mass and the
  stiffness of each storey were plotted in separated graphics
  due to their difference in order of magnitude.

  \begin{figure}[htb]
    \centering
    \begin{subfigure}{14cm}
      \includegraphics[width=\textwidth]{tmdfrf_importance1_14x5}
      \caption{Design point with $\beta=3.49$.}
      \label{fig:tmdfrf-importance1}
    \end{subfigure} \\
    \begin{subfigure}{14cm}
      \includegraphics[width=\textwidth]{tmdfrf_importance2_14x5}
      \caption{Design point with $\beta=4.07$.}
      \label{fig:tmdfrf-importance2}
    \end{subfigure}
    \caption{Importances vectors ($\*\alpha$) for each design point.}
  \end{figure}

  Next, the importances at each design point $\*x$, were
  plotted in fig.\ \ref{fig:tmdfrf-importance1} and
  \ref{fig:tmdfrf-importance2}. In both cases we notice that
  the random variables with greatest influence in the
  system's failure are $X_{21}$ and $X_{22}$, which correspond
  to the stiffness of the 1st and 2nd storeys $K_1$ and $K_2$,
  respectively.

  Having identified those two parameters with greatest importance,
  we then fixed $\*x*$, for each design point, and varied only
  $X_{21} (K_1)$ and $X_{22} (K_2)$ to get the failure
  surfaces in fig.\ \ref{fig:subfailsurf1} and \ref{fig:subfailsurf2}.

  Since the design points are close to one another and in
  both graphics the failure domain is slightly linear, then maybe
  the results obtained from FORM are representative. A crude
  Monte Carlo simulation will be carried on for comparison
  purposes.

  \begin{figure}[htb]
    \centering
    \begin{subfigure}{6cm}
      \includegraphics[width=\textwidth]{tmdfrf_subfailsurf1_6x6}
      \caption{Design point 1}
      \label{fig:subfailsurf1}
    \end{subfigure} ~
    \begin{subfigure}{6cm}
      \includegraphics[width=\textwidth]{tmdfrf_subfailsurf2_6x6}
      \caption{Design point 2}
      \label{fig:subfailsurf2}
    \end{subfigure}
      \caption{Failure surface fixing $X*$ and varying $X_{21}$ and
      $X_{22}$.}
  \end{figure}


\subsection{Crude Monte Carlo}
  In order to verify the results obtained from the FORM runs,
  a crude Monte Carlo simulation was used to estimate the
  probability of failure of the TMD. The numbers of random
  this time -- 40 variables -- is quite high, so there
  is no benefit in creating a surrogate model. Consequently,
  it was necessary to use a more permissive convergence
  criterion of $CoV = 0.10$, so that the method would not
  take too long.

  The method converged after 265000 evaluations to the value
  $P_f = \num{3.77e-4}$. In terms of order of magnitude, it
  is not too far from the
  value of $P_f = \num{2.4096e-4}$ at the first design
  point found by the FORM method.

  \begin{figure}[htb]
    \includegraphics[width=14cm]{tmdfrf_MCS_Pf_16x8}
  \end{figure}



