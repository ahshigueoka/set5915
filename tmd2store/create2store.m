function sysX = create2store(X, param)
    % Create a model from the parameters given defined by the given
    % instance of the random vector X and the deterministic parameters param.
    
    % Number of stores plus the TMD
    n = param.n + 1;

    % Each floor equivalent mass
    m1 = X(1);
    m2 = X(2);
    
    k = param.k;
    c = param.c;

    % TMD design
    r = 0.05;
    md = r * (m1 + m2);
    fopt = 1/(1 + r) * sqrt((2-r)/2);
    ksiopt = sqrt(3*r / 8 / (1 + r)) * sqrt((2-r)/2);
    omega_tmd = fopt*param.omega_n(1);
    kd = omega_tmd^2*md;
    cd = 2*omega_tmd*ksiopt*md;

    Minv = diag(  1./[m1, m2, md]  );

    Stiff = [   k(1)+k(2),        -k(2),       .0    ; ...
                -k(2),      k(2)+kd,      -kd    ; ...
                   .0,          -kd,       kd   ];

    Damp = [   c(1)+c(2),        -c(2),       .0    ; ...
                -c(2),      c(2)+cd,      -cd    ; ...
                   .0,          -cd,       cd   ];

    % Construct the state-space model
    A = [    zeros(n),     eye(n)     ; ...
              -Minv*Stiff,    -Minv*Damp    ];

    B = [   zeros(n)   ; ...
              Minv   ]/param.sd;

    % Normalize by static deflection, as explained in Zang, Friswell,
    % Mottershead 2005
    C = [      eye(n),      zeros(n)     ; ...
          -Minv*Stiff,    -Minv*Damp    ];

    D = [   zeros(n)   ; ...
              -Minv   ];

	sysX = ss(A, B, C, D);
end