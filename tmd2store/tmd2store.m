% Analyses the dynamical properties of a 2 store building.
close all
clear
clc

diary('tmd2store.out')

% Load the system's properties
param = tmd2store_params();

% Perform a basic modal analysis
tmd2store_modanl(param);

%-------------------------------------------------------------------------------
% FORM
% Start from the mean
tmd2store_FORM('limst_pos', param, [ 0.0,  0.0]);
% Start one standar deviation in each variable
tmd2store_FORM('limst_pos', param, [-1.0, -1.0]);

%-------------------------------------------------------------------------------
% Monte Carlo with importance sampling
% uses meta model
tmd2meta_IS('limst_pos_meta', param, [0.0, 0.0]);
% a different starting point leads to a different design point
tmd2meta_IS('limst_pos_meta', param, [-1.0, -1.0]);

%-------------------------------------------------------------------------------
% Monte Carlo Simulation using UQLAB with metamodeling
%
% Plot the surface response of the metamodel
tmd2store_show_spline_metamodel(param);
% Perform Monte Carlo simulation
tmd2meta_MCS('limst_pos_meta', param)

diary off