function Out = normat(In)
    mi = min(min(In));
    ma = max(max(In));
    
    Out = (In - mi)/(ma-mi);
end