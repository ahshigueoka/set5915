function test_importance_cdf()
    X = linspace(200, 1800, 1001);
    parametersX = [1080, 50, 855, 50];
    
    Z = uq_importance_cdf(X, parametersX);
    
    figure
    plot(X, Z);
    
    Y = linspace(200, 1800, 1001);
    parametersY = [748, 50, 1260, 50];
    
    [XX, YY] = meshgrid(X, Y);
    ZZ = uq_importance_cdf(XX, parametersX).*uq_importance_cdf(YY, parametersY);
    
    figure
    surf(XX, YY, ZZ);
    shading interp
end