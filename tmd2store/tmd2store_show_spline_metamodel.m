function tmd2store_show_spline_metamodel(param)
    lb = param.meta.lb;
    ub = param.meta.ub;
    num_samples = param.meta.num_samples;
    
    load(param.meta.filename, 'X1', 'X2', 'meshX1', 'meshX2', 'G');
    
    Y1 = linspace(lb(1), ub(1), 10*num_samples(1));
    Y2 = linspace(lb(2), ub(2), 10*num_samples(2));

    [meshY1, meshY2] = meshgrid(Y1, Y2);
    Gapprox = interp2(meshX1, meshX2, G, meshY1, meshY2, 'spline', -1);
    
    cteal = [ linspace(   8, 158, 128); ...
              linspace(  48, 202, 128); ...
              linspace( 107, 225, 128)]'/255;
    
    cblue  = [ linspace(   0,   0, 128); ...
               linspace(  32, 128, 128); ...
               linspace(  64, 255, 128)]'/255;

    cgreen = [ linspace(  0, 0, 128); ...
               linspace(  64, 255, 128); ...
               linspace(  32, 128, 128)]'/255;
    
    figure('Name', 'Original failure surface')
    surf(meshX1, meshX2, G, normat(G)*128)
    shading interp
    colormap(cteal)
    hold on
    patch([lb(1), ub(1), ub(1), lb(1)], ...
          [lb(2), lb(2), ub(2), ub(2)], ...
          [0, 0, 0, 0], ...
          [253,174,107]/255, ...
          'EdgeColor', 'none');
    hold off

    figure
    surf(meshX1, meshX2, G, normat(G)*128)
    hold on
    surf(meshY1, meshY2, Gapprox, normat(Gapprox)*128+128);

    colormap([cblue; cgreen])
    shading interp

    patch([lb(1), ub(1), ub(1), lb(1)], ...
          [lb(2), lb(2), ub(2), ub(2)], ...
          [0, 0, 0, 0], ...
          [253,174,107]/255, ...
          'EdgeColor', 'none');
    hold off
end