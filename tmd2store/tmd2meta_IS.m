function tmd2meta_IS(limst_name, param, st_point)
% Perform a Monte Carlo with importance sampling

    %---------------------------------------------------------------------------
    % Load metamodel data
    load(param.meta.filename, 'meshX1', 'meshX2', 'G');
    meta.meshX1 = meshX1;
    meta.meshX2 = meshX2;
    meta.G = G;
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = meta;
    modelopts.isVectorized = true;
    myModel = uq_createModel(modelopts);

    IOpts.Marginals(1).Name = 'M1';
    IOpts.Marginals(1).Type = 'Gaussian';
    IOpts.Marginals(1).Moments = [param.m(1), param.cov(1)*param.m(1)];
    IOpts.Marginals(1).Bounds = [param.meta.lb(1), param.meta.ub(2)];

    IOpts.Marginals(2).Name = 'M2';
    IOpts.Marginals(2).Type = 'Gaussian';
    IOpts.Marginals(2).Moments = [param.m(2), param.cov(2)*param.m(2)];
    IOpts.Marginals(2).Bounds = [param.meta.lb(2), param.meta.ub(2)];
    uq_setDefaultSampling('LHS');
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Preliminar run of the FORM method
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.FORM.StartingPoint = st_point;

    FORMAnalysis = uq_createAnalysis(FORMOpts);
    
    %---------------------------------------------------------------------------
    % Configure the importance sampling Monte Carlo simulation
    
    ISOpts.Type = 'Reliability';
    ISOpts.Method = 'IS';
    ISOpts.Simulation.TargetCoV = 0.01;
    ISOpts.Simulation.MaxSampleSize = 1e7;
    ISOpts.IS.FORM = FORMAnalysis;
    ISAnalysis = uq_createAnalysis(ISOpts);

    uq_print(ISAnalysis);
    uq_display(ISAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end