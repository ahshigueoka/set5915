function Y = uq_importance_pdf(X, par)
    num_dp = length(par) / 2;
    
    Y = zeros(size(X));
    
    for j = 1:num_dp
        Y = Y + pdf('Normal', X, par(2*j-1), par(2*j))/num_dp;
    end
end