function tmd2store_modanl(param)
    % Perform a modal analysis of the nominal system
    n = param.n;
    X = param.m;
    
    sys2store = create2store(X, param);
    sys2store_undamped = create2store_undamped(X, param);

    % Calculate the resonance frequencies and the mode shapes of the nominal
    % system.
    [V, evals] = eig(sys2store.A, 'vector');

    % Remove conjugate eigenvalues in order to avoid repeating resonance
    % frequencies and sort in ascending order of resonance frequencies
    posvals = evals((2*n-1):-2:1);
    Omega_d = imag(posvals);
    ModeShapes = V(1:n, (2*n-1):-2:1);

    for k = 1:n
        fprintf('Mode %d:\n', k)
        fprintf('    omega_n: %e\n', abs(posvals(k)));
        fprintf('    omega_d: %e\n', Omega_d(k));
    end

    % Value of the limit state function for the nominal system
    fprintf('G(m1, m2) = %e\n', limst_pos(X, param));

    %Plot mode shapes
    for k = 1:n
        figure('Name', sprintf('Mode %d', k), ...
            'PaperUnits', 'centimeters', ...
            'PaperSize', [4, 8], ...
            'PaperPosition', [0, 0, 4, 8], ...
            'PaperPositionMode', 'manual');
        vn = norm(ModeShapes(:, k));
        patch( [0; real(ModeShapes(:, k)); 0]/vn, ...
               [0; imag(ModeShapes(:, k)); 0]/vn, ...
               [0, 1:n, n], ...
               [2, 129, 138]/255, 'FaceAlpha', 0.3, ...
               'EdgeColor', 'none');
        hold on
        plot3( [0; real(ModeShapes(:, k))]/vn, ...
               [0; imag(ModeShapes(:, k))]/vn, ...
               [0, 1:n], '-s', 'Color', [2, 129, 138]/255,...
               'LineWidth', 3)
        hold off
        grid on
        axis equal
        axis([-1, 1, -1, 1, 0, n])
        view(45, 30)
    end

    wvec = logspace(0, 1.3, 1e3);
    
    FRF22_undamped = freqresp(sys2store_undamped(2, 2), wvec, 'Hz');
    FRF22_undamped = reshape(FRF22_undamped, size(wvec));
    FRF22_damped = freqresp(sys2store(2, 2), wvec, 'Hz');
    FRF22_damped = reshape(FRF22_damped, size(wvec));
    
    figure('Name', 'FRF22mag');
    magh = axes();
    semilogx(magh, wvec, 20*log10(abs(FRF22_undamped)), 'Color', [.0, .45, .74], 'Linewidth', 1)
    hold on
    semilogx(magh, wvec, 20*log10(abs(FRF22_damped)), '--','Color', [.85, .33, .10], 'Linewidth', 1);
    hold off
    ylabel('Magnitude')
    set(magh, 'XTickLabel', '')
    set(magh, 'XLim', [1, 20])
    set(magh, 'YLim', [-100, 40], 'YTick', -100:20:40)
    set(magh, 'MinorGridLineStyle', '-')
    set(magh, 'MinorGridColor', [.4, .4, .4])
    set(magh, 'MinorGridColorMode', 'manual');
    set(magh,'LooseInset',get(gca,'TightInset'))
    grid on
    
    figure('Name', 'FRF22phs')
    phsh = axes();
    semilogx(phsh, wvec, angle(FRF22_undamped)/pi*180, 'Color', [.0, .45, .74], 'Linewidth', 1)
    hold on
    semilogx(phsh, wvec, angle(FRF22_damped)/pi*180, '--', 'Color', [.85, .33, .10], 'Linewidth', 1);
    hold off
    ylabel('Phase')
    xlabel('Frequency (Hz)');
    set(phsh, 'XLim', [1, 20])
    set(phsh, 'YTick', -180:45:180, 'YLim', [-200, 20])
    set(phsh, 'MinorGridLineStyle', '-')
    set(phsh, 'MinorGridColor', [.4, .4, .4])
    set(phsh, 'MinorGridColorMode', 'manual');
    set(phsh,'LooseInset',get(gca,'TightInset'))
    grid on
end