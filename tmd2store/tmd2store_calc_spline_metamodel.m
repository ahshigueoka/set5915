function tmd2store_calc_spline_metamodel(param)
    
    lb = param.meta.lb;
    ub = param.meta.ub;
    num_samples = param.meta.num_samples;
    
    X1 = linspace(lb(1), ub(1), num_samples(1));
    X2 = linspace(lb(2), ub(2), num_samples(2));
    [meshX1, meshX2] = meshgrid(X1, X2);
    G = zeros(num_samples);

    for j = 1:num_samples(1)
        for k = 1:num_samples(2)
            G(j, k) = limst_pos([meshX1(j, k), meshX2(j, k)], param);
        end
    end

    save(param.meta.filename, 'X1', 'X2', 'meshX1', 'meshX2', 'G');

    
end