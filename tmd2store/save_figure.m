function save_figure(name, dims, flags)
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperPosition', [0, 0, dims]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperSize', dims);

    if flags(1) == 1
        print('-dpdf', sprintf('figures/%s.pdf', name));
    end
    if flags(2) == 1
        print('-dpng', '-r300', sprintf('figures/%s.png', name));
    end
end