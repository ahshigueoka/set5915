function param = tmd2store_params()

    % Number of degrees of freedom, excluding the tmd
    n = 2;
    
    % Mean mass of each floor
    m1 = 1e3; % 1 ton
    m2 = 1e3; % 1 ton
    
    % Coeffient of variation of each floor
    cov1 = 0.1;
    cov2 = 0.1;
    
    % % Each store equivalent stiffness
    k1 = 1e6; % 1 MN/m
    k2 = 1e6; % 1 MN/m
    
    % Extremely small values of damping just to avoid losing control of the
    % resonance peaks when plotting the FRF
    c1 = 1;
    c2 = 1;
    
    % Natural frequencies of the sole structure
    Me = [      m1,      0    ; ...
             0,     m2   ];
         
    Ke = [   k1+k2,    -k2    ; ...
               -k2,     k2   ];

    omega_n2 = eig(Ke, Me);
    param.omega_n = sqrt(omega_n2);

    % Create the struct of deterministic parameters to be given to the
    % limit state function
    % Number of stores
    param.n = n;
    % Each floor mean mass and coefficient of variation
    param.m = [m1, m2];
    param.lb = [m1 - 8*cov1*m1, m2 - 8*cov2*m2];
    param.ub = [m1 + 8*cov1*m1, m2 + 8*cov2*m2];
    param.cov = [cov1, cov2];
    % Each store equivalent stiffness
    param.k = [k1, k2];
    % Each store equivalent damping
    param.c = [c1, c2];
    % Maximum allowed normalized deflection
    param.Amax = 10;
    % Frequency interval where the peak amplitude of the FRF will be searched
    % for
    param.wspan = [14, 24];
    % Static deflection of the last store (series association of springs)
    param.sd = sum(1./param.k);
    
    % Number of samples points in the spline metamodel
    param.meta.num_samples = [101, 101];
    % Lower and upper bounds to be used in the metamodel. Values outside
    % this range will result in -1 for the limit state function
    param.meta.lb = [m1 - 8*cov1*m1, m2 - 8*cov2*m2];
    param.meta.ub = [m1 + 8*cov1*m1, m2 + 8*cov2*m2];
    % The name of the file where the metamodel will be stored
    param.meta.filename = 'tmd2store_metamodel.mat';
    
end