function Y = limst_pos_meta(X, param)
    % Create the system model corresponding to this instance of the random
    % variable X.
    
    Y = interp2(param.meshX1, param.meshX2, param.G, ...
        X(:, 1), X(:, 2), 'spline', -1);
    
end