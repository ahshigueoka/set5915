% Calculates the design point for several starting conditions
close all
clear
clc

diary('tmd2store_FORM_attractors.out')

mySeed = 3740921;
fprintf('Using uniform number generator with seed: %d\n', mySeed);

rng(mySeed)

param = tmd2store_params();

st_points = (rand(100, param.n)*2-1)*5;

tmdn_FORM_attractors('limst_pos', param, st_points);

diary off