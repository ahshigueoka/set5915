function sysX = create2store_undamped(X, param)
    % Create a model from the parameters given defined by the given
    % instance of the random vector X and the deterministic parameters param.
    
    % Number of stores
    n = param.n;

    % Each floor equivalent mass
    m1 = X(1);
    m2 = X(2);
    
    k = param.k;
    c = param.c;

    Minv = diag(  1./[m1, m2]  );

    Stiff = [  k(1)+k(2),      -k(2)    ; ...
                   -k(2),       k(2)   ];

    Damp = [   c(1)+c(2),      -c(2)    ; ...
                   -c(2),       c(2)   ];

    % Construct the state-space model
    A = [    zeros(n),     eye(n)     ; ...
              -Minv*Stiff,    -Minv*Damp    ];

    B = [   zeros(n)   ; ...
              Minv   ] / param.sd;

    % Normalize by static deflection, as explained in Zang, Friswell,
    % Mottershead 2005
    C = [      eye(n),      zeros(n)     ; ...
          -Minv*Stiff,    -Minv*Damp    ];

    D = [   zeros(n)   ; ...
              -Minv   ];

	sysX = ss(A, B, C, D);
end