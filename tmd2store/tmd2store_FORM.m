function tmd2store_FORM(limst_name, param, st_point)
% Use FORM on the original tmd model.
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    IOpts.Marginals(1).Name = 'M1';
    IOpts.Marginals(1).Type = 'Gaussian';
    IOpts.Marginals(1).Moments = [param.m(1), param.cov(1)*param.m(1)];
    IOpts.Marginals(1).Bounds = [param.meta.lb(1), param.meta.ub(2)];

    IOpts.Marginals(2).Name = 'M2';
    IOpts.Marginals(2).Type = 'Gaussian';
    IOpts.Marginals(2).Moments = [param.m(2), param.cov(2)*param.m(2)];
    IOpts.Marginals(2).Bounds = [param.meta.lb(2), param.meta.ub(2)];
    uq_setDefaultSampling('LHS');
    myInput = uq_createInput(IOpts);

    %---------------------------------------------------------------------------
    % FORM analysis
    %
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.FORM.StartingPoint = st_point;

    FORMAnalysis = uq_createAnalysis(FORMOpts);

    uq_print(FORMAnalysis);
    uq_display(FORMAnalysis);

    set(gcf, ...
        'PaperUnits', 'centimeters', ...
        'PaperSize', [14, 10], ...
        'PaperPosition', [0, 0, 14, 10], ...
        'PaperPositionMode', 'manual');
end