function V = uq_importance_invcdf(U, parameters)
    persistent importance_invcdf_X importance_invcdf_Y
    
    if isempty(importance_invcdf_X)
        lb = min(parameters(1:2:end) - 6*parameters(2:2:end));
        ub = max(parameters(1:2:end) + 6*parameters(2:2:end));
        importance_invcdf_X = linspace(lb, ub, 10001);
        importance_invcdf_Y = uq_importance_cdf(importance_invcdf_X, parameters);
    end
    
    V = interp1(importance_invcdf_Y, importance_invcdf_X, U, 'spline', 'extrap');
end