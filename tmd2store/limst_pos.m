function Y = limst_pos(X, param)
    % Create the system model corresponding to this instance of the random
    % variable X.
    
    sysX = create2store(X, param);
    
    % Find the greates peak of the FRF in the region around the former first
    % resonance frequency.
    wvec = linspace(param.wspan(1), param.wspan(2), 100);
    
    % Calculate the FRF of the system
    H = freqresp(sysX, wvec);
    
    % Consider only the amplitude of the FRF
    Hmag = abs(H(1:2, 1:2, :));
    
    % Pick the maximum of every FRF
    Hmax = max(max(max(Hmag, [], 3)));
    
    % Subtract the acceptable vibration amplitude
    Y = param.Amax - Hmax;
end