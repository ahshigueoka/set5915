function tmd2meta_MCS(limst_name, param)
% Perform a crude Monte Carlo analysis on tmd2

    %---------------------------------------------------------------------------
    % Load metamodel data
    load(param.meta.filename, 'meshX1', 'meshX2', 'G');
    meta.meshX1 = meshX1;
    meta.meshX2 = meshX2;
    meta.G = G;
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = meta;
    modelopts.isVectorized = true;
    myModel = uq_createModel(modelopts);

    IOpts.Marginals(1).Name = 'M1';
    IOpts.Marginals(1).Type = 'Gaussian';
    IOpts.Marginals(1).Moments = [param.m(1), param.cov(1)*param.m(1)];
    IOpts.Marginals(1).Bounds = [param.meta.lb(1), param.meta.ub(2)];

    IOpts.Marginals(2).Name = 'M2';
    IOpts.Marginals(2).Type = 'Gaussian';
    IOpts.Marginals(2).Moments = [param.m(2), param.cov(2)*param.m(2)];
    IOpts.Marginals(2).Bounds = [param.meta.lb(2), param.meta.ub(2)];
    uq_setDefaultSampling('LHS');
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the crude Monte Carlo simulation
    
    MCSOpts.Type = 'Reliability';
    MCSOpts.Method = 'MCS';
    MCSOpts.Simulation.TargetCoV = 0.01;
    MCSOpts.Simulation.MaxSampleSize = 1e7;
    MCSAnalysis = uq_createAnalysis(MCSOpts);

    uq_print(MCSAnalysis);
    uq_display(MCSAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end