function tmdfrf_subfailsurf(Xs, params)
    
    numsamp1 = 101;
    numsamp2 = 101;
    
    X21 = linspace(params.mu(21) - 5*params.sig(21), ...
                   params.mu(21) + 5*params.sig(21), ...
                   numsamp1);
    X22 = linspace(params.mu(22) - 5*params.sig(22), ...
                   params.mu(22) + 5*params.sig(22), ...
                   numsamp2);
    [mesh21, mesh22] = meshgrid(X21, X22);
    
	G = zeros(numsamp1, numsamp2);

    X = Xs;
    for j = 1:numsamp1
        for k = 1:numsamp2
            X(21) = mesh21(j, k);
            X(22) = mesh22(j, k);
            G(j, k) = tmdfrf_limst(X, params);
        end
    end
    
    figure('Name', 'Failure subsurface for X21 and X22');
    surf(mesh21, mesh22, G);
    
    yellowcmap = [ linspace(.8, 1, 128);...
                   linspace(.6, 1, 128);...
                   linspace(.3, .1, 128)]';
    
    colormap(yellowcmap)
    caxis([0 .4])
    shading interp
    hold on
    patch([  X21(1), X21(end), X21(end),   X21(1)], ...
          [  X22(1),   X22(1), X22(end), X22(end)], ...
          [0, 0, 0, 0], [ 166,  54,   3]/255, ...
          'EdgeColor', 'none')
      
    plot3(params.mu(21), params.mu(22), 1, 'k+', 'LineWidth', 1)
    plot3(Xs(21), Xs(22), 1, 'ko', 'MarkerFaceColor', [1, 1, 1], 'LineWidth', 1)
    
    hold off
    
    set(gca, 'XLim', [X21(1), X21(end)], ...
             'YLim', [X22(1), X22(end)], ...
             'ZLim', [-1 1])
    view(0, 90)
end