function FORMAnalysis = tmdfrf_FORM_attractors(limst_name, params, st_points)
% Perform a FORM analysis on tmdfrf for a given initial condition

    %---------------------------------------------------------------------------
    % Create the UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = params;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);
    
    for j = 1:params.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
        IOpts.Marginals(j).Bounds = [params.lb(j), params.ub(j)];
    end
    
    for j = (1:params.n) + params.n
        IOpts.Marginals(j).Name = sprintf('K%d', j);
        IOpts.Marginals(j).Type = 'Lognormal';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the FORM method
    
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.Display = 'verbose';
    FORMOpts.FORM.StartingPoint = zeros(1, 2*params.n);
    
    local_mins = zeros(size(st_points));
    beta_mins = zeros(1, size(st_points, 1));
    
    %---------------------------------------------------------------------------
    % Execute FORM for each different starting point
    num_st_points = size(st_points, 1);
    
    for k = 1:num_st_points
        fprintf('\n========================================\n')
        fprintf('Iteration k=%6d\n', k)
        FORMOpts.FORM.StartingPoint = st_points(k, :);
        FORMAnalysis = uq_createAnalysis(FORMOpts);
        save(sprintf('FORMAnalysis%d.mat', k), 'FORMAnalysis')
        local_mins(k, :) = FORMAnalysis.Results.Xstar;
        beta_mins(k) = FORMAnalysis.Results.BetaHL;
        fprintf('----------------------------------------\n')
        fprintf('Results of iteration k=%6d\n', k)
        uq_print(FORMAnalysis);
    end
    
    %---------------------------------------------------------------------------
    % Plot the local minima
    figure('Name', 'Results of several FORM - Mj');
    axh = axes('NextPlot', 'add');
    %labelarray = strsplit(sprintf('%d ', 1:params.n));
    %labelarray(end) = [];
    
    % Use one different color for each design point
    for k = 1:num_st_points
        scatter(local_mins(k, 1:params.n), ...
            beta_mins(k)*ones(1, params.n), 'o')
    end
    
    set(axh, 'YLim', [0, 10])
    xlabel('M_j')
    ylabel('\beta_{HL}')
    
    figure('Name', 'Results of several FORM - Kj');
    axh = axes('NextPlot', 'add');
    
    % Use one different color for each design point
    for k = 1:num_st_points
        scatter(local_mins(k, (1:params.n) + params.n), ...
            beta_mins(k)*ones(1, params.n), 'o')
    end
    
    set(axh, 'YLim', [0, 10])
    xlabel('K_j')
    ylabel('\beta_{HL}')
    
end