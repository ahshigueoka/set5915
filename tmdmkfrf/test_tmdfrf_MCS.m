close all
clear
clc

diary('tmdfrf_MCS.out')

%-------------------------------------------------------------------------------
% Load the system properties
params = tmdfrf_params();

%-------------------------------------------------------------------------------
% Crude Monte Carlo
tmdfrf_MCS('tmdfrf_limst', params)

diary off