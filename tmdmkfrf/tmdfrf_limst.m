function Y = tmdfrf_limst(X, params)
%TMDFRF_LIMST compute the limit-state function of a system realization
%
%

    sysX = create_tmdfrf(X, params);
    
    N = params.n + 1;
    A = [          zeros(N),               eye(N);
          -sysX.Minv*sysX.K, -sysX.Minv*sysX.Damp ];
    B = [ zeros(size(sysX.B)); sysX.B ];
    % Displacement relative to x(1) (except x1)
    C = [eye(N), zeros(N)];
    C(:, 1) = -ones(N, 1);
    C(1, 1) = 1;
    
    D = zeros(N, size(B, 2));

    stsys = prescale(ss(A, B, C, D));
    
%     minopt = optimset(...
%         'Display', 'iter', ...
%         'FunValCheck', 'on', ...
%         'MaxFunEvals', 1000, ...
%         'MaxIter', 1000, ...
%         'PlotFcns', @optimplotfval, ...
%         'TolX', 1e-8);
    
    minopt = optimset(...
          'Display', 'off', ...
          'FunValCheck', 'off', ...
          'MaxFunEvals', 1000, ...
          'MaxIter', 1000, ...
          'TolX', 1e-8);
      
    objfun = @(w) -abs(freqresp(stsys(20, 1), w));
    [~, fval1] = fminbnd(objfun, 1, 12, minopt);
    [~, fval2] = fminbnd(objfun, 12, 20, minopt);
    
%     wvec = logspace(0, 1.3, 2e4);
%     FRF = freqresp(stsys(20, 1), wvec);
%     FRF = reshape(FRF, size(wvec));
%     FRFmag = abs(FRF);
%     FRFphs = unwrap(angle(FRF));
%     
%     figure('Name', 'FRF storey 20')
%     subplot(2, 1, 1)
%     semilogy(wvec, FRFmag)
%     subplot(2, 1, 2)
%     plot(wvec, FRFphs)
    
    Y = params.Amax + 0.03*min([fval1, fval2]);
end

