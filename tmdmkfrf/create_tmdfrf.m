function sysX = create_tmdfrf(X, params)
% State-space representation of LTI model of the building
%
% This function accepts the random design parameters and the deterministic
% design parameters in struct params, makes a LTI state space
% representation of the building.
%
% Input of the LTI model: acceleration at the base of the building.
% Output of the LTI model: differential displacement of each floor relative
%     to the base of the building.
%
    % TMD design, for base excitation
    r = params.r;
    md = r * sum(params.mu(1:20));
    fopt = 1/(1 + r) * sqrt((2-r)/2);
    ksiopt = sqrt(3*r / 8 / (1 + r)) * sqrt((2-r)/2);
    omega_tmd = fopt*params.omega_n(1);
    kd = omega_tmd^2*md;
    cd = 2*omega_tmd*ksiopt*md;
    
    % Separate mass and stiffness values from the random vector X
    % Include base excitation and the TMD
    Mj = [            X(1:params.n), md ];
    Cj = [                 params.c, cd ];
    Kj = [ X((1:params.n)+params.n), kd ];
    
    % Create the mass matrix
    Minv = diag(1./Mj);
    
    % Create the stifness and damping matrices, augmented so as to include
    % the TMD
    C = zeros(params.n + 1);
    K = zeros(params.n + 1);
    
    % Assemble the stiffness matrix
    for j = 1:params.n
        C = assembleMat(C, Cj(j+1), j, j+1, params.n + 1);
        K = assembleMat(K, Kj(j+1), j, j+1, params.n + 1);
    end
    
    % Do the correction for displacement input
    C(1, 2) = 0;
    K(1, 2) = 0;
    
    sysX.Minv = Minv;
    sysX.Damp = C;
    sysX.K = K;
    sysX.B = zeros(params.n + 1, 2);
    sysX.B(1, 1) = Kj(1);
    sysX.B(1, 2) = Cj(1);
    sysX.B = Minv*sysX.B;
end