function FORMAnalysis = tmdfrf_FORM(limst_name, params, st_point)
% Perform a FORM analysis on tmdfrf for a given initial condition

    %---------------------------------------------------------------------------
    % Create the UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = params;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);
    
    for j = 1:params.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
        IOpts.Marginals(j).Bounds = [params.lb(j), params.ub(j)];
    end
    
    for j = (1:params.n) + params.n
        IOpts.Marginals(j).Name = sprintf('K%d', j);
        IOpts.Marginals(j).Type = 'Lognormal';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the FORM method
    
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.Display = 'verbose';
    FORMOpts.FORM.StartingPoint = st_point;
    
    %---------------------------------------------------------------------------
    % Execute FORM
    FORMAnalysis = uq_createAnalysis(FORMOpts);
    
    uq_display(FORMAnalysis);
end