function integrate_elcentro()
% Integrate the acceleration profile of El Centro to obtain the
% displacement and the speed time histories.
%

    options = odeset('RelTol', 1e-8, 'AbsTol', 1e-10);
    q0 = [0; 0];
    tspan = linspace(0, 31.18, 1560);
    
    [~, q] = ode45(@elcentro_stspace, tspan, q0, options);
    
    figure('Name', 'El Centro displacement history')
    plot(tspan, q(:, 1))
    
    figure('Name', 'El Centro speed history')
    plot(tspan, q(:, 2))
    
    Fs = 50;
    Ts = 1/Fs;
    L = 1560;
    f = Fs*(0:L-1)/L;
    
    Y = fft(q(:, 1));
    P2 = abs(Y/L);
    
    figure('Name', 'FT of displacement')
    plot(f, P2);
    
    save('elcentro_int.mat', 'tspan', 'q');
end