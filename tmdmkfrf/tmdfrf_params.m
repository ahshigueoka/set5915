function params = tmdfrf_params()
    % Number of stores
    n = 20;

    % Bosse & Beck case study
    % Each floor's nominal equivalent mass
    mj = [ 28440, 28440, 28440, 28440, ...
           26010, 26010, 26010, 26010, ...
           23760, 23760, 23760, ...
           21690, 21690, 21690, ...
           19800, 19800, 19800, ...
           18450, 18450, 18450]*1.05;

    kj = [ .8234, .8234, .8234, .8234, ...
           .6122, .6122, .6122, .6122, ...
           .4444, .4444, .4444, ...
           .3138, .3138, .3138, ...
           .2143, .2143, .2143, ...
           .2143, .2143, .2143]*1e9*1.18;
      
    % Very small values of daping to avoid losing control of resonance peaks
    % when plotting the FRF
    cj = 1e-6*ones(1, n);

    % Calculate the global matrices of the structure without the TMD
    Mg = diag(mj);
    Kg = zeros(n);

    % Assemble the global stiffness matrix
    for j = 1:n
        Kg = assembleMat(Kg, kj(j), j-1, j, n);
    end

    % Calculate the natural frequencies
    [ModeShapes, omega2] = eig(Kg, Mg, 'chol', 'vector');
    omega_n = sqrt(omega2);

    for j = 1:6
        fprintf('f_%d: %e Hz\n', j, omega_n(j) / 2 / pi);
    end

    %figure
    %plot([zeros(1, 4); ModeShapes(:, 1:4)])
    
    % Create the struct of deterministic parameters to be given to the
    % limit state function
    % Number of stores
    params.n = n;
    
    % Each the mean and the standard deviation of each floor's mass and
    % equivalent stiffness, as well as lower and upper bounds
    params.mu = [mj, kj];
    cov = [0.10*ones(size(mj)), 0.15*ones(size(kj))];
    params.sig = cov .* params.mu;
    params.lb = params.mu - 8*params.sig;
    params.ub = params.mu + 8*params.sig;
    
    % Each store equivalent damping
    params.c = cj;
    % Maximum allowed normalized deflection
    params.Amax = 0.36;
    % Frequency interval where the peak amplitude of the FRF will be searched
    % for
    params.wspan = [7, 16];
    % Static deflection of the last store (series association of springs)
    params.sd = sum(1./kj);
    % Natural frequencies of the structures
    params.omega_n = omega_n;
    
    % TMD mass ratio to the structure's total mass
    params.r = 0.1;
end