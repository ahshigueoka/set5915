close all
clear
clc

diary('tmdfrf_FORM_attractors.out')

mySeed = 3740921;
fprintf('Using uniform number generator with seed: %d\n', mySeed);

rng(mySeed)

params = tmdfrf_params();

% Generate 100 starting vectors with values between -3 and 3
st_points = (rand(100, 2*params.n)*2-1)*3;

tmdfrf_FORM_attractors('tmdfrf_limst', params, st_points);

diary off