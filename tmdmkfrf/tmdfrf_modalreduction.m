function ssTMD = tmdfrf_modalreduction(sysX, modnum)
% Perform a modal reduction of the building system with the TMD.
% 
% Only the modnum modes with lowest natural frequencies will be kept,
% in order to avoid aliasing when simulating the model in the time domain.
    N = size(sysX.K, 1);
    
    A = [          zeros(N),                 eye(N)    ; ...
          -sysX.Minv*sysX.K,  -sysX.Minv*sysX.Damp    ];
    B = [ zeros(size(sysX.B)); sysX.B ];
    C = [eye(N), zeros(N)];
    D = [];
    
    % Calculate the eigenvectors and associated eigenvalues of the system.
    [V, egv] = eig(A, 'vector');
    
    % Reorder the modes so that the lowest frequencies come first
    [egv, idx] = sort(egv);
    V = V(:, idx);
    Vinv = inv(V);
    
    % Separate the eigenvectors corresponding to the modnum modes with the
    % lowest natural frequencies. Do not forget that a mode and its
    % conjugate must be included.
    Phi = V(:, 1:2*modnum);
    Phiinv = Vinv(1:2*modnum, :);
    
    Omega = egv(1:2*modnum);
    
    % Create the modal reduced system
    A = real(Phi*diag(Omega)*Phiinv);
    
    ssTMD = ss(A, B, C, []);
end