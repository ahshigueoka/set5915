function MCSAnalysis = tmdfrf_MCS(limst_name, params)
% Perform a crude Monte Carlo analysis on tmd2
    
    % Save the time when this simulation started
    simdate = datestr(datetime('now'), 30);
    %---------------------------------------------------------------------------
    % Create the UQLAB model and inputs
    % Create the model
    uq_setDefaultSampling('LHS');
    modelopts.mFile = limst_name;
    modelopts.Parameters = params;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);
    
    for j = 1:params.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
        IOpts.Marginals(j).Bounds = [params.lb(j), params.ub(j)];
    end
    
    for j = (1:params.n) + params.n
        IOpts.Marginals(j).Name = sprintf('K%d', j);
        IOpts.Marginals(j).Type = 'Lognormal';
        IOpts.Marginals(j).Moments = [params.mu(j), params.sig(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the crude Monte Carlo simulation
    
    MCSOpts.Type = 'Reliability';
    MCSOpts.Method = 'MCS';
    MCSOpts.Display = 'verbose';
    MCSOpts.Simulation.TargetCoV = 0.10;
    MCSOpts.Simulation.BatchSize = 1e3;
    MCSOpts.Simulation.MaxSampleSize = 1e6;
    MCSAnalysis = uq_createAnalysis(MCSOpts);

    uq_print(MCSAnalysis);
    uq_display(MCSAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
    
    save(sprintf('tmdfrf_MCS_res_%s.mat', simdate), 'MCSAnalysis')
end