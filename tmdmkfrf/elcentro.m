function acc = elcentro(t)
    persistent ecdata;
    
    if isempty(ecdata)
        ecdata = dlmread('elcentro.dat', '\t');
    end
    
    acc = interp1(ecdata(:, 1), ecdata(:, 2), t, 'linear', 0);
end