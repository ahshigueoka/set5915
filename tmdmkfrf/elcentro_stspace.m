function Xp = elcentro_stspace(t, X)
    Xp = [0; 0];
    
    Xp(1) = X(2);
    Xp(2) = 9.81*elcentro(t);
end