close all
clear
clc

% Construct the model  of a 20-store building and then perform a FORM
% analysis on it

diary('tmdnstore.out')

param = tmdnstore_params();
tmdnstore_FORM('limst_tmdn', param);

diary off