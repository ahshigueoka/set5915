function tmdnstore_FORM_red(limst_name, param)
% Perform a FORM analysis on tmdn, with only the two most significant
% variables
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    IOpts.Marginals(1).Name = 'M1';
    IOpts.Marginals(1).Type = 'Gaussian';
    IOpts.Marginals(1).Moments = [param.m(param.n-1), param.cov(param.n-1)*param.m(param.n-1)];
    IOpts.Marginals(1).Bounds = [param.lb(param.n-1), param.ub(param.n-1)];
    
    IOpts.Marginals(2).Name = 'M2';
    IOpts.Marginals(2).Type = 'Gaussian';
    IOpts.Marginals(2).Moments = [param.m(param.n), param.cov(param.n)*param.m(param.n)];
    IOpts.Marginals(2).Bounds = [param.lb(param.n), param.ub(param.n)];
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the FORM method
    
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.FORM.StartingPoint = [0.0, 0.0];
    FORMAnalysis = uq_createAnalysis(FORMOpts);

    uq_print(FORMAnalysis);
    uq_display(FORMAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end