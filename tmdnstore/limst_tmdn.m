function Y = limst_tmdn(X, param)
    % Create the system model corresponding to this instance of the random
    % variable X.
    
    sysX = createnstore(X, param);
    
    % Find the greatest peak of the FRF in the region around the former first
    % resonance frequency.
    wvec = linspace(param.wspan(1), param.wspan(2), 100);
    
    % Calculate the FRF of the system at the last floor, where the
    % amplitude is the largest
    H = freqresp(sysX(param.n, param.n), wvec);
    
    % Consider only the amplitude of the FRF
    Hmag = abs(H);
    
    % Pick the maximum of every FRF
    Hmax = max(max(max(Hmag, [], 3)));
    
    % Subtract the acceptable vibration amplitude
    Y = param.Amax - Hmax;
end