function tmdnstore_IS(limst_name, param)
% Perform a FORM analysis on tmdn
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    for j = 1:param.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [param.m(j), param.cov(j)*param.m(j)];
        IOpts.Marginals(j).Bounds = [param.lb(j), param.ub(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the Monte Carlo method method importance sampling
    
    ISOpts.Type = 'Reliability';
    ISOpts.Method = 'IS';
    ISOpts.Simulation.TargetCoV = 0.05;
    ISOpts.Simulation.MaxSampleSize = 1e5;
    ISAnalysis = uq_createAnalysis(ISOpts);

    uq_print(ISAnalysis);
    uq_display(ISAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end