function tmdnstore_FORM(limst_name, param)
% Perform a FORM analysis on tmdn
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    for j = 1:param.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [param.mu(j), param.sig(j)];
        IOpts.Marginals(j).Bounds = [param.lb(j), param.ub(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the FORM method
    
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.FORM.StartingPoint = zeros(1, param.n);
    FORMAnalysis = uq_createAnalysis(FORMOpts);

    uq_print(FORMAnalysis);
    uq_display(FORMAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end