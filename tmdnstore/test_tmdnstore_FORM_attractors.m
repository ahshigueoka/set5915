close all
clear
clc

diary('tmdnstore_FORM_attractors.out')

mySeed = 3740921;
fprintf('Using uniform number generator with seed: %d\n', mySeed);

rng(mySeed)

param = tmdnstore_params();

st_points = (rand(100, param.n)*2-1)*5;

tmdn_FORM_attractors('limst_tmdn', param, st_points);

diary off