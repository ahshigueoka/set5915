function tmdn_FORM_attractors(limst_name, param, st_points)
% Perform a FORM analysis on tmdn for several initial conditions and check
% the attractors
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    for j = 1:param.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [param.mu(j), param.sig(j)];
        IOpts.Marginals(j).Bounds = [param.lb(j), param.ub(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the FORM method
    
    FORMOpts.Type = 'Reliability';
    FORMOpts.Method = 'FORM';
    FORMOpts.FORM.StartingPoint = zeros(1, param.n);
    
    local_mins = zeros(size(st_points));
    beta_mins = zeros(1, size(st_points, 1));
    
    %---------------------------------------------------------------------------
    % Execute FORM for each different starting point
    num_st_points = size(st_points, 1);
    for k = 1:num_st_points
        FORMOpts.FORM.StartingPoint = st_points(k, :);
        FORMAnalysis = uq_createAnalysis(FORMOpts);
        local_mins(k, :) = FORMAnalysis.Results.Xstar;
        beta_mins(k) = FORMAnalysis.Results.BetaHL;
        fprintf('========================================\n')
        fprintf('Results of iteration k=%6d\n', k)
        uq_print(FORMAnalysis);
        fprintf('========================================\n\n')
    end
    
    %---------------------------------------------------------------------------
    % Plot the local minima
    figh = figure('Name', 'Results several FORMs');
    axh = axes('NextPlot', 'add');
    labelarray = strread(num2str(1:param.n), '%s');
    % Use one different color for each random variable
    for k = 1:num_st_points
        scatter(local_mins(k, :), beta_mins(k)*ones(1, param.n), 'o', ...
            'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'flat')
        text(local_mins(k, :), beta_mins(k)*ones(1, param.n), labelarray)
    end
    
    set(axh, 'YLim', [0, 10])
    xlabel('M_j')
    ylabel('\beta_{HL}')
    colormap('colorcube');
end