function tmdnstore_modanl()
% Modal analysis of the nominal system
    %------------------------------------------------------------------------------
    % Analysis of the nominal system
    %
    % Number of stores
    n = 20;

    % Bosse & Beck case study
    % Each floor's nominal equivalent mass
    mj = [ 28440, 28440, 28440, 28440, ...
          26010, 26010, 26010, 26010, ...
          23760, 23760, 23760, ...
          21690, 21690, 21690, ...
          19800, 19800, 19800, ...
          18450, 18450, 18450];

    kj = [ .8234, .8234, .8234, .8234, ...
          .6122, .6122, .6122, .6122, ...
          .4444, .4444, .4444, ...
          .3138, .3138, .3138, ...
          .2143, .2143, .2143, ...
          .2143, .2143, .2143]*1e9;

    % Pourzeynali, Lavasani, Modarayi, 2006
    % mj = [  2.15, 2.01, 2.01, ...
    %         2.00, 2.01, 2.01, ...
    %         2.01, 2.03, 2.03, ...
    %         2.03, 1.76]*1e5;
    % kj = [  4.68, 4.76, 4.68, ...
    %         4.50, 4.50, 4.50, ...
    %         4.50, 4.37, 4.37, ...
    %         4.37, 3.12]*1e8;


    % Very small values of daping to avoid losing control of resonance peaks
    % when plotting the FRF
    cj = .5*1e-2*ones(1, n);

    % Calculate the global matrices of the structure without the TMD
    Mg = diag(mj);
    Kg = zeros(n);

    % Assemble the global stiffness matrix
    for j = 1:n
        Kg = assembleMat(Kg, kj(j), j-1, j, n);
    end

    % Calculate the natural frequencies
    [V, D] = eig(Kg, Mg, 'qz', 'vector');
    [omega2, idx] = sort(D);
    ModeShapes = V(:, idx);
    omega_n = sqrt(omega2);

    for j = 1:6
        fprintf('f_%d: %e Hz\n', j, omega_n(j) / 2 / pi);
    end

    figure
    
    for j = 1:6
        subplot(2, 3, j)
        plot([0; ModeShapes(:, j)], 0:20, '-s')
        text(0, 21, sprintf('Mode %d', j), 'HorizontalAlignment', 'center')
        axis([-2, 2, 0, 22])
    end
    
end