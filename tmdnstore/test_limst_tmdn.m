function test_limst_tmdn()
    param = tmdnstore_params();
    
    mj = [ 28440, 28440, 28440, 28440, ...
           26010, 26010, 26010, 26010, ...
           23760, 23760, 23760, ...
           21690, 21690, 21690, ...
           19800, 19800, 19800, ...
           18450];
  
    samp1 = 20;
    samp2 = 20;
    M1 = linspace(300, 100000, samp1);
    M2 = linspace(300, 100000, samp2);
    
    [mesh1, mesh2] = meshgrid(M1, M2);
    
    G = zeros(samp1, samp2);
    
    for j = 1:samp1
        for k = 1:samp2
            G(j, k) = limst_tmdn([mj, mesh1(j, k), mesh2(j, k)], param);
        end
    end
    
    figure
    surf(mesh1, mesh2, G);
    shading interp
    
    hold on
    
    patch([300, 100000, 100000, 300], ...
          [300, 300, 100000, 100000], ...
          [0, 0, 0, 0], [253,174,107]/255)
    
    hold off
end