function sysX = createnstore(X, param)
% Create a model from the parameters given defined by the given
% instance of the random vector X and the deterministic parameters param.
    
    % Number of stores. Include one more for the tmd
    n = param.n;

    % Each floor equivalent mass
    m = X;
    
    k = param.k;
    c = param.c;

    % TMD design
    r = 0.05;
    md = r * sum(m);
    fopt = 1/(1 + r);% * sqrt((2-r)/2);
    ksiopt = sqrt(3*r / 8 / (1 + r));% * sqrt((2-r)/2);
    omega_tmd = fopt*param.omega_n(1);
    kd = omega_tmd^2*md;
    cd = 2*omega_tmd*ksiopt*md;

    % Include the TMD to the structure
    n = n+1;
    m = [m, md];
    k = [k, kd];
    c = [c, cd];
    
    Minv = diag(1./m);
    Kg = zeros(n);
    
    % Assemble the global stiffness matrix
    for j = 1:n
        Kg = assembleMat(Kg, k(j), j-1, j, n);
    end

    % Assemble the global damping matrix
    Damp = zeros(n);
    for j = 1:n
        Damp = assembleMat(Damp, c(j), j-1, j, n);
    end

    % Construct the state-space model
    A = [    zeros(n),     eye(n)     ; ...
              -Minv*Kg,    -Minv*Damp    ];

    B = [   zeros(n)   ; ...
              -Minv   ] / param.sd;

    % Normalize by static deflection, as explained in Zang, Friswell,
    % Mottershead 2005
    C = [      eye(n),      zeros(n)     ; ...
          -Minv*Kg,    -Minv*Damp    ];

    D = [   zeros(n)   ; ...
              -Minv   ];

	sysX = ss(A, B, C, D);
end