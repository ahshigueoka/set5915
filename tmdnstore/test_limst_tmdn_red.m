function test_limst_tmdn_red()
    param = tmdnstore_params();
    
    N = 101;
    M1 = linspace(10, 10000, N);
    M2 = linspace(10, 10000, N);
    
    [mesh1, mesh2] = meshgrid(M1, M2);
    
    Z = zeros(N);
    
    for j = 1:N
        for k = 1:N
            Z(j, k) = limst_tmdn_red([mesh1(j, k), mesh2(j, k)], param);
        end
    end
    
    figure
    surf(mesh1, mesh2, Z);
    shading interp
    
    cteal = [ linspace(   8, 222, 128); ...
              linspace(  48, 235, 128); ...
              linspace( 107, 247, 128)]'/255;
    colormap(cteal)
end