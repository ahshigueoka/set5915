function tmdnstore_MCS(limst_name, param)
    % Perform a crude Monte Carlo analysis on tmdn
    
    %---------------------------------------------------------------------------
    % Create UQLAB model and inputs
    % Create the model
    modelopts.mFile = limst_name;
    modelopts.Parameters = param;
    modelopts.isVectorized = false;
    myModel = uq_createModel(modelopts);

    for j = 1:param.n
        IOpts.Marginals(j).Name = sprintf('M%d', j);
        IOpts.Marginals(j).Type = 'Gaussian';
        IOpts.Marginals(j).Moments = [param.m(j), param.cov(j)*param.m(j)];
        IOpts.Marginals(j).Bounds = [param.lb(j), param.ub(j)];
    end
    
    myInput = uq_createInput(IOpts);
    
    %---------------------------------------------------------------------------
    % Configure the crude Monte Carlo simulation
    
    MCSOpts.Type = 'Reliability';
    MCSOpts.Method = 'MCS';
    MCSOpts.Simulation.TargetCoV = 0.01;
    MCSOpts.Simulation.BatchSize = 1e3;
    MCSOpts.Simulation.MaxSampleSize = 1e4;
    MCSAnalysis = uq_createAnalysis(MCSOpts);

    uq_print(MCSAnalysis);
    uq_display(MCSAnalysis);
    set(gcf, ...
        'PaperUnits', 'inches', ...
        'PaperSize', [10, 8], ...
        'PaperPosition', [0, 0, 10, 8], ...
        'PaperPositionMode', 'manual');
end