function K = assembleMat(K, val, j, k, sz)
    if (0 < j) && (j <= sz)
        K(j, j) = K(j, j) + val;
        if (0 < k) && (k <= sz)
            K(j, k) = K(j, k) - val;
            K(k, j) = K(k, j) - val;
        end
    end
    if (0 < k) && (k <= sz)
        K(k, k) = K(k, k) + val;
    end
end