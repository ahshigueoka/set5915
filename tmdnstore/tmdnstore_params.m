function param = tmdnstore_params()
    % Number of stores
    n = 20;

    % Bosse & Beck case study
    % Each floor's nominal equivalent mass
    mj = [ 28440, 28440, 28440, 28440, ...
          26010, 26010, 26010, 26010, ...
          23760, 23760, 23760, ...
          21690, 21690, 21690, ...
          19800, 19800, 19800, ...
          18450, 18450, 18450];

    kj = [ .8234, .8234, .8234, .8234, ...
          .6122, .6122, .6122, .6122, ...
          .4444, .4444, .4444, ...
          .3138, .3138, .3138, ...
          .2143, .2143, .2143, ...
          .2143, .2143, .2143]*1e9;
      
    % Very small values of daping to avoid losing control of resonance peaks
    % when plotting the FRF
    cj = ones(1, n);

    % Calculate the global matrices of the structure without the TMD
    Mg = diag(mj);
    Kg = zeros(n);

    % Assemble the global stiffness matrix
    for j = 1:n
        Kg = assembleMat(Kg, kj(j), j-1, j, n);
    end

    % Calculate the natural frequencies
    [ModeShapes, omega2] = eig(Kg, Mg, 'chol', 'vector');
    omega_n = sqrt(omega2);

    for j = 1:6
        fprintf('f_%d: %e Hz\n', j, omega_n(j) / 2 / pi);
    end

    figure
    plot([zeros(1, 4); ModeShapes(:, 1:4)])
    colormap(cool)
    
    % Create the struct of deterministic parameters to be given to the
    % limit state function
    % Number of stores
    param.n = n;
    
    % Each the mean and the standard deviation of each floor's mass and
    % equivalent stiffness, as well as lower and upper bounds
    param.mu = mj;
    cov = 0.10*ones(size(mj));
    param.sig = cov .* param.mu;
    param.lb = param.mu - 8*param.sig;
    param.ub = param.mu + 8*param.sig;
    
    % Each store equivalent damping
    param.c = cj;
    % Each store equivalent stiffness
    param.k = kj;
    % Maximum allowed normalized deflection
    param.Amax = 10;
    % Frequency interval where the peak amplitude of the FRF will be searched
    % for
    param.wspan = [7, 16];
    % Static deflection of the last store (series association of springs)
    param.sd = sum(1./kj);
    % Natural frequencies of the structures
    param.omega_n = omega_n;
end