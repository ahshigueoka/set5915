function sysX = create_tmdless(X, param)
% Create a model from the parameters given defined by the given
% instance of the random vector X and the deterministic parameters param.
    
    nDOF = param.n;
    ns = param.n-1;
    m = [param.m(1), X(1:ns)];
    k = [X((1:ns) + ns) .0];
    c = [param.c, .0];
    
    % Assemble the global stiffness matrix with damping
    Kg = spalloc(nDOF, nDOF, 3*nDOF);
    for j = 1:nDOF
        Kg = assembleMat(Kg, k(j), j, j+1, nDOF);
    end
    
    % Global mass matrix
    Minv = sparse(1:nDOF, 1:nDOF, 1./m);

    % Assemble the global damping matrix
    Damp = spalloc(nDOF, nDOF, 3*nDOF);
    for j = 1:nDOF
        Damp = assembleMat(Damp, c(j), j, j+1, nDOF);
    end
    
    % Add boundary condition of base excitation
    Kg(1, :) = zeros(1, nDOF);
    Damp(1, :) = zeros(1, nDOF);
    
    % Vector of input forces
    B = zeros(nDOF, 1);
    % Base excitation
    B(1) = param.m(1);
    
    % Return the global matrices
    sysX.K = Kg;
    sysX.Minv = Minv;
    sysX.Damp = Damp;
    sysX.B = B;
end