close all
clear
clc

diary('tmdmk_FORM_attractors.out')

mySeed = 3740921;
fprintf('Using uniform number generator with seed: %d\n', mySeed);

rng(mySeed)

param = tmdmk_params();

st_points = (rand(100, 2*(param.n-1))*2-1)*5;

tmdmk_FORM_attractors('limst_tmdmk', param, st_points);

diary off