function Xp = tmdmk_stspace(t, X, sysX)
    n = size(X, 1) / 2;
    
    Xp = zeros(size(X));
    
    Xp(1:n) = X((1:n) + n);
    
    Xp((1:n) + n) = -sysX.Minv*sysX.K*X(1:n) - sysX.Minv*sysX.Damp*X((1:n)+n)...
        ...+ sysX.Minv*3*cos(2*pi*1.95*t)*sysX.B;
        + sysX.Minv*9.81*elcentro(t)*sysX.B;
end