function tmdmk_animation(X, param)
% Create the system model corresponding to this instance of the random
% variable X.
    sysQ = create_tmdmk(X, param);
    
    % Calculate the time response of the structure
    dinfun = @(t, q) tmdmk_stspace(t, q, sysQ);
    
    t_vec = linspace(0, 32, 1601);
    
    q0 = zeros(2*(param.n+1), 1);
    %x =ode2(dinfun, t_vec, q0);
    options = odeset('RelTol', 1e-3, 'AbsTol', 1e-4);
    [~, x] = ode45(dinfun, t_vec, q0, options);
    
    figure
    plot(t_vec, x(:, 1:(param.n+1)));
    
    num_frames = 1001;
    mov(num_frames) = struct('cdata',[],'colormap',[]);
    
    figh = figure('Name', 'TMD');
    axh = axes('XLim', [-1, 1], 'YLim', [0, 22], 'NextPlot', 'replaceChildren');
    
    for j = 1:num_frames
        plot(x(j, 1:(param.n+1))', 1:param.n+1, '-s', 'LineWidth', 3)
        drawnow
        mov(j) = getframe(figh);
    end
    
    myVideo = VideoWriter('tmd.mp4', 'MPEG-4');
    myVideo.FrameRate = 30;
    open(myVideo)
    writeVideo(myVideo, mov)
    close(myVideo)
end