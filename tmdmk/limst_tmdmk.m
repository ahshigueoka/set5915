function Y = limst_tmdmk(X, param)
    % Create the system model corresponding to this instance of the random
    % variable X.
    
    sysX = create_tmdmk(X, param);
    
    % Calculate the time response of the structure
    dinfun = @(t, q) tmdmk_stspace(t, q, sysX);
    
    t_vec = linspace(param.tspan(1), param.tspan(2), param.tsamples);
    q0 = zeros(2*(param.n+1), 1);
    options = odeset('RelTol', 1e-6, 'AbsTol', 1e-8);
    [~, x] = ode45(dinfun, t_vec, q0, options);
    
    %figure
    %plot(t_vec, x(:, 1:(param.n+1)));
    
    %figure
    %plot(t_vec, diff(x(:, 1:(param.n+1)), 1, 2)')
    
%     lncolor = [ linspace(.9290, .4660, 20); ...
%                 linspace(.6940, .6740, 20); ...
%                 linspace(.1250, .1880, 20)]';
%     
%     colormap(lncolor)
    
    % Limit state equation of the storeys relative to the ground
    Ampdisp = 0;
    
    for k = 2:(param.n+1)
        Ampdisp = max(Ampdisp, max(abs(x(:, k) - x(:, 1))));
    end
    
    %disp(Ampdisp)
    
    % Limit state equation of relative displacement between floors
    diffdisp = diff(x(:, 1:(param.n+1)), 1, 2);
    
    %Maximum amplitude values
    Ampdiff = max(abs(diffdisp));
    %disp(Ampdiff');
    
    Y = zeros(1, param.n+1);

    Y(1, 1:param.n) = param.Amaxdiff - Ampdiff;
    Y(1, param.n + 1) = param.Amax - Ampdisp;
end