function param = tmdmk_params()
    % Number of stores, including base, but not including the TMD
    n = 21;

    % Bosse & Beck case study
    % Each floor's nominal equivalent mass
    mj = [28440, ...
          28440, 28440, 28440, 28440, ...
          26010, 26010, 26010, 26010, ...
          23760, 23760, 23760, ...
          21690, 21690, 21690, ...
          19800, 19800, 19800, ...
          18450, 18450, 18450];

    % Each storey's stiffness
    kj = [ .8234, .8234, .8234, .8234, ...
          .6122, .6122, .6122, .6122, ...
          .4444, .4444, .4444, ...
          .3138, .3138, .3138, ...
          .2143, .2143, .2143, ...
          .2143, .2143, .2143, .0]*1e9;
      
    % Very small values of damping to avoid losing control of resonance peaks
    % when plotting the FRF
    cj = 100*ones(1, n-1);

    % Calculate the global matrices of the structure without the TMD
    Mg = diag(mj);
    Kg = zeros(n);

    % Assemble the global stiffness matrix
    for j = 1:(n-1)
        Kg = assembleMat(Kg, kj(j), j, j+1, n);
    end
    
    % Apply boundary conditions
    Kg(1, :) = [];
    Kg(:, 1) = [];
    Mg(1, :) = [];
    Mg(:, 1) = [];

    % Calculate the natural frequencies
    [ModeShapes, omega2] = eig(Kg, Mg, 'chol', 'vector');
    omega_n = sqrt(omega2);

    for j = 1:6
        fprintf('f_%d: %e Hz\n', j, omega_n(j) / 2 / pi);
    end

    %figure
    %plot(ModeShapes(:, 1:4))
    %colormap(cool)
    
    % Create the struct of deterministic parameters to be given to the
    % limit state function
    % Number of stores
    param.n = n;
    
    param.num_modes = 6;
    % Each store mean and cov for mass and stiffness
    param.means = [mj(2:n), kj(1:(n-1))];
    param.cov = [0.10*ones(1, n-1), 0.15*ones(1, n-1)];
    param.lb = param.means - 8*param.cov.*param.means;
    param.ub = param.means + 8*param.cov.*param.means;
    
    % Each store equivalent damping
    param.c = cj;
    % Maximum allowed displacement for any floor
    % height / 200
    param.Amax = 0.36; %m
    % Maximum allowed displacement between floors
    % distance between floors / 200
    param.Amaxdiff = 0.018*ones(1, n); %m
    % Maximum allowed displacement between the last floor and the TMD
    param.Amaxdiff(n) = 0.20; %m
    % Frequency interval where the peak amplitude of the FRF will be searched
    % for
    param.tspan = [0, 32];
    param.tsamples = 1601;
    % Static deflection of the last store (series association of springs)
    param.sd = sum(1./kj(1:(n-1)));
    % Natural frequencies of the structures
    param.omega_n = omega_n;
    
end