function sysX = create_tmdmk(X, param)
% Create a model from the parameters given defined by the given
% instance of the random vector X and the deterministic parameters param.

    % TMD design, for base excitation
    r = 0.1;
    md = r * sum(param.means(1:20));
    fopt = 1/(1 + r) * sqrt((2-r)/2);
    ksiopt = sqrt(3*r / 8 / (1 + r)) * sqrt((2-r)/2);
    omega_tmd = fopt*param.omega_n(1);
    kd = omega_tmd^2*md;
    cd = 2*omega_tmd*ksiopt*md;
    

    % Include the TMD to the structure
    nDOF = param.n+1;
    ns = param.n-1;
    m = [param.means(1), X(1:ns), md];
    k = [X((1:ns) + ns), kd, .0];
    c = [param.c, cd, .0];
    
    % Assemble the global stiffness matrix with damping
    Kg = spalloc(nDOF, nDOF, 3*nDOF);
    for j = 1:nDOF
        Kg = assembleMat(Kg, k(j), j, j+1, nDOF);
    end
    
    % Global mass matrix
    Minv = sparse(1:nDOF, 1:nDOF, 1./m);

    % Assemble the global damping matrix
    Damp = spalloc(nDOF, nDOF, 3*nDOF);
    for j = 1:nDOF
        Damp = assembleMat(Damp, c(j), j, j+1, nDOF);
    end
    
    % Add boundary condition of base excitation
    Kg(1, :) = zeros(1, nDOF);
    Damp(1, :) = zeros(1, nDOF);
    
    % Vector of input forces
    B = zeros(nDOF, 1);
    % Base excitation
    B(1) = param.means(1);
    
    % Return the global matrices
    sysX.K = Kg;
    sysX.Minv = Minv;
    sysX.Damp = Damp;
    sysX.B = B;
end