# README #

### What is this repository for? ###

* Quick summary
Used to store the code used to develop the course assignement of SET5915 -- Confiabilidade Estrutural
* Version
0.1

### How do I get set up? ###

* Summary of set up
This code uses the [UQLAB][] toolbox from ETH -- Zurich

* Dependencies
Just download UQLAB, obtain the license `uq_license.p`, place it in the `core` folder and run
the file `uqlab_install.p`. For more details, see UQLAB's documentation.

* How to run tests

### Who do I talk to? ###

* Repo owner or admin
Augusto Hirao Shigueoka: augusto.shigueoka@usp.br

### References
[UQLAB](http://www.uqlab.com/)
